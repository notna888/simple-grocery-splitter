function getCalcVal(row){
  let calculatedVal;

  // Check if 'otherData' is a number
  if (typeof row.otherData === 'number') {
    calculatedVal = row.otherData;
  } else if (row.otherData === "") {
    calculatedVal = 0
  } else {
    // Try evaluating the expression in 'otherData'
    try {
      calculatedVal = eval(row.otherData);
    } catch (error) {
      console.error("Error evaluating expression:", row.otherData, error);
      calculatedVal = null; // Handle errors gracefully (e.g., set to null)
    }
  }
  return calculatedVal
}


const app = Vue.createApp({
  data() {
    return {
      rows: [
        { name: "total", otherData: "", isSpecial: true, isTotal: true },
        { name: "shared equally", otherData: "", isSpecial: true, isShared: true }
      ],
      calculatedDict: {},
    };
  },
  methods: {
    calculateVals() {
      const calculatedDict = {};
      let inputVals = {};
      let proportionDict = {};
      let sharedTotal = 0;
      let totalVal = 0;
      let peopleCount = 0;
      let sumOfOthers = 0;
      let totalToSplit = 0;

      for (const row of this.rows) {
        let calculatedVal;

        calculatedVal = getCalcVal(row)

        if(row.isTotal){
          totalVal += calculatedVal;
        } else if (row.isShared) {
          sharedTotal += calculatedVal;
        } else {
          inputVals[row.name] = calculatedVal;
          peopleCount += 1
        }
      }

      inputTotal = Object.values(inputVals).reduce((sum, value) => sum + value, 0)
      totalToSplit = totalVal - sharedTotal

      // console.log('Total to split ' + totalToSplit)
      // console.log('////////////')

      for (let [k, v] of Object.entries(inputVals)) {
        proportionDict[k] = v / inputTotal;
      }

      if(peopleCount > 0){
        sharedVal = sharedTotal / peopleCount
        for (let [k, v] of Object.entries(proportionDict)) {
          // console.log('val ' + v)
          // console.log('inputTotal ' + inputTotal)
          // console.log('sharedVal' + sharedVal)
          // console.log('################')
          calculatedDict[k] = (parseFloat(v * totalToSplit) + parseFloat(sharedVal)).toFixed(2);
        }

        this.calculatedDict = calculatedDict

      }
    },
    addNewRow() {
      this.rows.push({ name: "", otherData: "" });
    },
    removeRow(index) {
      if (!this.rows[index].isSpecial) {
        this.rows.splice(index, 1);
      }
    },
    resetOtherData() {
      // Loop through rows and reset only the "otherData" property
      this.rows.forEach(row => {
        row.otherData = "";
      });
    },
  },
});

app.mount("#app");
