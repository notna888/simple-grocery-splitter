const app = Vue.createApp({
  data() {
    return {
      rows: [
        { name: "total", otherData: "", isTotal: true }
      ],
      calculatedDict: {},
    };
  },
  methods: {
    calculateVals() {
      const calculatedDict = {};
      let sharedTotal = 0;
      let totalVal = 0;
      let peopleCount = 0;
      let sumOfOthers = 0;

      for (const row of this.rows) {
        let calculatedVal;

        // Check if 'otherData' is a number
        if (typeof row.otherData === 'number') {
          calculatedVal = row.otherData;
        } else if (row.otherData === "") {
          calculatedVal = 0
        } else {
          // Try evaluating the expression in 'otherData'
          try {
            calculatedVal = eval(row.otherData);
          } catch (error) {
            console.error("Error evaluating expression:", row.otherData, error);
            calculatedVal = null; // Handle errors gracefully (e.g., set to null)
          }
        }

        if(row.isTotal){
          totalVal += calculatedVal;
        } else {
          calculatedDict[row.name] = calculatedVal;
          peopleCount += 1
        }
      }

      sumOfOthers = Object.values(calculatedDict).reduce((sum, value) => sum + value, 0)

      sharedTotal = totalVal - sumOfOthers


      if(peopleCount > 0){
        let amountToAdd = (sharedTotal / peopleCount).toFixed(2);
        let amountNotCovered = sharedTotal - (amountToAdd * peopleCount);

        // Handle the first item (optional, based on your requirements)
        if (Object.keys(calculatedDict).length > 0) {
          curVal = parseFloat(calculatedDict[Object.keys(calculatedDict)[0]])
          calculatedDict[Object.keys(calculatedDict)[0]] = (curVal + amountNotCovered).toFixed(2);
        }

        for (const [name, value] of Object.entries(calculatedDict)) {
          calculatedDict[name] = (parseFloat(value) + parseFloat(amountToAdd)).toFixed(2);
        }

        this.calculatedDict = calculatedDict
      }
    },
    addNewRow() {
      this.rows.push({ name: "", otherData: "" });
    },
    removeRow(index) {
      if (!this.rows[index].isTotal) {
        this.rows.splice(index, 1);
      }
    },
    resetOtherData() {
      // Loop through rows and reset only the "otherData" property
      this.rows.forEach(row => {
        row.otherData = "";
      });
    },
  },
});

app.mount("#app");
